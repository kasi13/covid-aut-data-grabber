const express = require('express')

const app = express()

const worker = require('./worker');

var bodyParser = require('body-parser');
var target;
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));

(function(){

	target = process.argv.slice(2) + "";

	console.clear();
	console.log("\x1b[1m\x1b[32m", "########################################################################################################","\x1b[0m");
	console.log("\x1b[1m\x1b[32m", "Booting up: " + new Date(), "\x1b[0m");
	console.log("\x1b[1m\x1b[32m", target == "" ? "No Target File" : "Target File: " + target,"\x1b[0m");
	target.replace(/\\/g, "\\\\");
	worker.worker('main', target);
})();

app.get('/', function(req, res)
{
	worker.worker('main', target);
});

app.get('/index/', function(req, res)
{
	res.render('index');
});

app.listen(81, () => console.log(' Covid Tracker startup successful!'));