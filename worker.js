var wrkr = {};

var layout = {"colorway":['#eb5e34', '#ebdc34', '#b1eb34'], "paper_bgcolor":"#333333", "plot_bgcolor":"#333333", "font":{"color": "#FFFFFF"}};

var data = {};
var plotData1 = {"layout": layout, "data": [{"x": [], "y":[], stackgroup: 'one', name:"Todesfaelle"},{"x": [], "y":[], stackgroup: 'one', name:"Resterkrankte"},{"x": [], "y":[], stackgroup: 'one', name:"Genesen"}]};
var plotData2 = {"layout": layout, "data": [{"x": [], "y":[], stackgroup: 'one', name:"Neuinfektionen"},{"x": [], "y":[], stackgroup: 'two', name:"Delta Vortag"}]};
var imgOpts = {
    format: 'png',
    width: 1920,
    height: 1080
};

const https = require('https');
const unzip = require('unzipper');
const csv = require('csv-parser');
const Excel = require('exceljs');
var plotly = require('plotly')('felix.kasi13','WWAoqU0QWqmeDTLn0f21');
const fs = require('fs');


var workbook = new Excel.Workbook();
var target;

var total = 0;
var totalGenesen = 0;
var totalTodesfealle = 0;

module.exports = {

	worker: function(call, content = null, callback = null){
		target = content;
		wrkr[call](content, function(ret){
			callback(ret);
		});
	}
}

wrkr.main = function(content, callback)
{
	wrkr.getData(function(ret){
		wrkr.unzipData(ret);
	});
}

wrkr.getData = function(callback)
{
	https.get("https://covid19-dashboard.ages.at/data/data.zip", function(response) {
  	callback(response);
	});
}

wrkr.unzipData = async function(content)
{

	const map = ["CovidFaelleDelta.csv"];

	content.pipe(unzip.Parse())
  .on('entry', function (entry) {
    const fileName = entry.path;
    const type = entry.type; // 'Directory' or 'File'
    const size = entry.size; // There is also compressedSize;
    if (map.includes(fileName)) {
      wrkr.processData(entry.path,entry);
    } else {
      entry.autodrain();
    }
  }).on('close', function(){
  	wrkr.cleanupData(function(){
	  	//console.log(data);
	  	wrkr.outputData(function(){
	  		plot(plotData1, "Epikurve", function(){
	  			plot(plotData2, "Entwicklung", function(){
		  			console.error("\x1b[1m\x1b[32m","DONE - WINDOW CLOSES AUTOMATICALLY","\x1b[0m");
		  			console.log("\x1b[1m\x1b[32m", "########################################################################################################","\x1b[0m");
		    		setTimeout(function(){process.exit(0)},1000);
	  			});
	  		});
    	});
  	});
  });
}


function plot(data, saveAs, callback){
	console.log(" plotting " + saveAs);
	plotly.getImage(data, imgOpts, function (error, imageStream) {
	    if (error) return console.log ("error:" + error);

	    var fileStream = fs.createWriteStream(saveAs + '.png');
	    imageStream.pipe(fileStream);
	    imageStream.on("end", function(){
	    	callback();
	    });
	});
}

wrkr.processData = function(fileName, content, callback)
{
	var temp = {};
	content
  .pipe(csv())
  .on('data', (row) => {
  	for (x in row)
  	{
  		var line = row[x].split(';');
  		var date = new Date(line[0].split('.')[2].split(' ')[0],line[0].split('.')[1]-1,line[0].split('.')[0], 12);
  		var Epikurve = line[1];
  		var genesen = line[2];
  		var todesfaelle = line[3];
  		var deltaVortag = line[4];

  		date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

  		if(data[date.toJSON()] == undefined)data[date.toJSON()] = {};
  		data[date.toJSON()]["Epikurve"] = parseInt(Epikurve);
  		data[date.toJSON()]["deltaGenesen"] = parseInt(genesen);
  		data[date.toJSON()]["deltaTodesfaelle"] = parseInt(todesfaelle);
  		data[date.toJSON()]["deltaAktivVortag"] = parseInt(deltaVortag);
  		//console.log(data);
  	}
  })
  .on('end', () => {
    console.log(' CSV file successfully processed');
  });
}

wrkr.cleanupData = function(callback)
{
	for(set in data)
	{
		if(data[set]["Epikurve"] == undefined)
		{
			delete data[set];
			break;
		}
		else
		{
			if(data[set]["Epikurve"] != undefined && data[set]["deltaGenesen"] == undefined)
			{
				data[set]["deltaGenesen"] = 0;
			}
			if(data[set]["Epikurve"] != undefined && data[set]["deltaTodesfaelle"] == undefined)
			{
				data[set]["deltaTodesfaelle"] = 0;
			}

			total += data[set]["Epikurve"];
			totalGenesen += data[set]["deltaGenesen"];
			totalTodesfealle += data[set]["deltaTodesfaelle"];

			data[set]["total"] = total;
			data[set]["totalGenesen"] = totalGenesen;
			data[set]["totalTodesfealle"] = totalTodesfealle;

			data[set]["rest"] = total - totalGenesen - totalTodesfealle;

			var c = new Date(set);
			var d = new Date(c-86400000).toJSON();

			data[set]["prGen"] = data[set]["totalGenesen"] / total;
			data[set]["prDea"] = data[set]["totalTodesfealle"] / total;
		}
	}
	callback();
}

wrkr.outputData = async function(callback)
{
	//console.log(data);
	if(target != "")
	{
		let workbook = new Excel.Workbook();
	  await workbook.xlsx.readFile(target);
	  let worksheet = workbook.getWorksheet(1);
		await (function(){
			var i = 2;
			for(set in data)
		  	{
			  	let date = new Date(set);
			  	let dateString = (date.getDate() < 10 ? "0"+ date.getDate() : date.getDate()) + "." + ((date.getMonth()+1) < 10 ? "0"+ (date.getMonth()+1) : (date.getMonth()+1)) + "." + date.getFullYear();
			    let row = worksheet.getRow(i++);
			    row.getCell(1).value = dateString;
			    row.getCell(2).value = data[set]["Epikurve"];
			    row.getCell(3).value = data[set]["total"];
			    row.getCell(4).value = data[set]["totalGenesen"];
			    row.getCell(5).value = data[set]["totalTodesfealle"];
			    row.getCell(6).value = data[set]["rest"];
			    row.getCell(7).value = data[set]["deltaAktivVortag"];
			    row.getCell(8).value = data[set]["prGen"];
			    row.getCell(9).value = data[set]["prDea"];
			    row.commit();


			    plotData1.data[0].x.push(dateString);
			    plotData1.data[0].y.push(data[set]["totalTodesfealle"]);
			    plotData1.data[1].x.push(dateString);
			    plotData1.data[1].y.push(data[set]["rest"]);
			    plotData1.data[2].x.push(dateString);
			    plotData1.data[2].y.push(data[set]["totalGenesen"]);

			    plotData2.data[0].x.push(dateString);
			    plotData2.data[0].y.push(data[set]["Epikurve"]);
			    plotData2.data[1].x.push(dateString);
			    plotData2.data[1].y.push(data[set]["deltaAktivVortag"]);
		  	}

		})();
		await workbook.xlsx.writeFile(target);
	}
	callback();
}